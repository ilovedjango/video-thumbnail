from django.db import models

# Create your models here.


class Post(models.Model):
    title = models.CharField(max_length=255)
    video = models.FileField(upload_to='post/video/%Y/%m/%d/', null=True, blank=True)  # NOQA
    thumbnail = models.FileField(upload_to='post/thumbnail/%Y/%m/%d/', null=True, blank=True)  # NOQA