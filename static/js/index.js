var dataURL = null
function takeThumbnail() {

    //generate thumbnail URL data
    var video = document.getElementById('video');
    var canvas = document.getElementById('canvas');

    var context = canvas.getContext('2d');

    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;

    context.drawImage(video, 0, 0, canvas.width, canvas.height);
    dataURL = canvas.toDataURL();

    // hidding model
    $("#videoModal").modal('hide');

    //create img
    var img = document.createElement('img');
    img.setAttribute('src', dataURL);
    img.setAttribute('width','200px')
    //append img in thumbnail div
    $('#thumbnailDiv').html(img);
}


$("#createPostForm").submit(function (event) {
    event.preventDefault();

    var formData = new FormData($('form')[0]);
    formData.append('csrfmiddlewaretoken', $.cookie("csrftoken"));
    formData.append('thumbnail', dataURL)
    $.ajax({
        url: CREATE_POST_URL,
        type: "post",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            console.log('success', data);
            alert(data)
            location.reload();
        }, error: function (rs, e) {
            alert(rs.status)
        }, complete: function () {
            console.log('request completed')
        }
    }); // end ajax
});

$(document).ready(function () {
    $("#file").change(function () {
        $("#videoModal").modal('show');
        $('.video-preview').attr('src', URL.createObjectURL(this.files[0]));
    });
});