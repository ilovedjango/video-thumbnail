from api import views
from django.conf.urls import url

app_name = 'api'

urlpatterns = [
    url(r'^post/', views.PostView.as_view(), name="post"),
]
