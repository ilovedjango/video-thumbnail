import base64

import time
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from post.models import Post
from api.serializer import PostSerializer


class PostView(APIView):
    queryset = Post.objects.none()
    serializer_class = PostSerializer
    http_method_names = ['post', ]

    def post(self, request):
        title = request.POST.get('title', None)
        video = request.FILES['file']
        thumbnail = request.POST.get('thumbnail', None)
        format, imgstr = thumbnail.split(';base64,')
        ext = format.split('/')[-1]
        data = ContentFile(base64.b64decode(imgstr))
        myfile = "post/thumbnail/thumbnail-" + time.strftime("%Y-%m-%d-%H-%M-%S") + "." + ext  # NOQA
        fs = FileSystemStorage()
        filename = fs.save(myfile, data)
        Post.objects.create(title=title, video=video, thumbnail=filename)
        return Response("post saved successfully", status=status.HTTP_201_CREATED)  # NOQA
